const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Matches_data = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Matches_data.push(data))
  .on("end", () => {
    const season = {};

    for (let index = 0; index < Matches_data.length; index++) {
      let year = Matches_data[index]["season"];

      if (season[year]) {
        season[year]++;
      } else {
        season[year] = 1;
      }
    }

    fs.writeFileSync(
      "../public/output/1-matches-per-year.json",
      JSON.stringify(season, null, 4)
    );
  });
