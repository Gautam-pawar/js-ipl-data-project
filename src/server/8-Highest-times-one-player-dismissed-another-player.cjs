const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Deliveries_data = [];

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Deliveries_data.push(data))
  .on("end", () => {
    let arr = [];

    const mymap = new Map();
    let max = 0;
    let maxs = "";
    for (let index = 0; index < Deliveries_data.length; index++) {
      let count = 1;
      let bowler = Deliveries_data[index]["bowler"];

      let Player_Dismissed = Deliveries_data[index]["player_dismissed"];

      let str = bowler + " & " + Player_Dismissed;
      count = 1;
      if (Player_Dismissed.length > 1) {
        if (mymap.has(str)) {
          let temp = mymap.get(str);
          temp++;
          if (max < temp) {
            max = temp;
            maxs = str;
          }

          mymap.set(str, temp);
        } else {
          mymap.set(str, 1);
        }
      }
    }

    const player_dismissed_another_player = {};

    player_dismissed_another_player[maxs] = max;

    fs.writeFileSync(
      "../public/output/8-Highest-times-one-player-dismissed-another-player.json",
      JSON.stringify(player_dismissed_another_player, null, 4)
    );
  });
