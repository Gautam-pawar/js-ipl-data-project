const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Matches_data = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Matches_data.push(data))
  .on("end", () => {
    let top_player_of_the_match = {};

    for (let index = 0; index < Matches_data.length; index++) {
      let player_of_match = Matches_data[index]["player_of_match"];
      let season = Matches_data[index]["season"];

      if (top_player_of_the_match[season]) {
        if (top_player_of_the_match[season][player_of_match]) {
          top_player_of_the_match[season][player_of_match]++;
        } else {
          top_player_of_the_match[season][player_of_match] = 1;
        }
      } else {
        let year = {};

        year[player_of_match] = 1;

        top_player_of_the_match[season] = year;
      }
    }

    const Top_players = {};

    for (let key in top_player_of_the_match) {
      obj = top_player_of_the_match[key];

      let max = 0;

      for (let key2 in obj) {
        if (obj[key2] > max) {
          max = obj[key2];
        }
      }

      const NT_player = {};

      for (let key in obj) {
        if (obj[key] === max) {
          NT_player[key] = max;
        }
      }

      Top_players[key] = NT_player;
    }

    fs.writeFileSync(
      "../public/output/6-top-player-of-the-match-per-season.json",
      JSON.stringify(Top_players, null, 4)
    );
  });
