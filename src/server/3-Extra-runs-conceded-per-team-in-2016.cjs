const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Matches_data = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Matches_data.push(data))
  .on("end", () => {
    const Season_data = [];
    for (
      let Matches_index = 0;
      Matches_index < Matches_data.length;
      Matches_index++
    ) {
      let id = Matches_data[Matches_index]["id"];

      let year = Matches_data[Matches_index]["season"];

      if (year === "2016") {
        Season_data.push(id);
      }
    }

    // console.log(Season_data);

    const Deliveries_data = [];

    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => Deliveries_data.push(data))
      .on("end", () => {
        const Extra_runs_conceded_2016 = {};

        for (
          let deliveries_index = 0;
          deliveries_index < Deliveries_data.length;
          deliveries_index++
        ) {
          let Match_id = Deliveries_data[deliveries_index]["match_id"];

          let team = Deliveries_data[deliveries_index]["bowling_team"];

          let extra_runs = parseInt(
            Deliveries_data[deliveries_index]["extra_runs"]
          );

          if (Season_data.includes(Match_id)) {
            if (Extra_runs_conceded_2016[team]) {
              Extra_runs_conceded_2016[team] += extra_runs;
            } else {
              Extra_runs_conceded_2016[team] = extra_runs;
            }
          }
        }

        const Total_Extra_runs_by_teams_in_2016 = {};

        Total_Extra_runs_by_teams_in_2016[2016] = Extra_runs_conceded_2016;

        fs.writeFileSync(
          "../public/output/3-Extra-runs-conceded-per-team-in-2016.json",
          JSON.stringify(Total_Extra_runs_by_teams_in_2016, null, 4)
        );
      });
  });
