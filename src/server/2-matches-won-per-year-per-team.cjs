const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Matches_data = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Matches_data.push(data))
  .on("end", () => {
    const winnersperyear = {};

    for (let index = 0; index < Matches_data.length; index++) {
      let season = Matches_data[index]["season"];
      let winner = Matches_data[index]["winner"];
      if (winnersperyear[season]) {
        let team = winnersperyear[season];
        if (team[winner]) {
          team[winner]++;
        } else {
          team[winner] = 1;
        }
      } else {
        let winningteam = {};
        winningteam[winner] = 1;
        winnersperyear[season] = winningteam;
      }
    }

    fs.writeFileSync(
      "../public/output/2-matches-won-per-year-per-team.json",
      JSON.stringify(winnersperyear, null, 4)
    );
  });
