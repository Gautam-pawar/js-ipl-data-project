const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Matches_data = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Matches_data.push(data))
  .on("end", () => {
    season_data = {};

    for (
      let Match_index = 0;
      Match_index < Matches_data.length;
      Match_index++
    ) {
      let season = Matches_data[Match_index]["season"];
      let id = Matches_data[Match_index]["id"];

      if (season_data[season]) {
        let temp = season_data[season];

        temp[1] = id;

        season_data[season] = temp;
      } else {
        let arr = new Array(2);

        arr[0] = id;

        arr[1] = 0;

        season_data[season] = arr;
      }
    }

    const Deliveries_data = [];

    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv({ separator: "," }))
      .on("data", (data) => Deliveries_data.push(data))
      .on("end", () => {
        const Batsmans_strikerate_per_season = {};

        for (const key in season_data) {
          if (Batsmans_strikerate_per_season[key] === undefined) {
            let start = parseInt(season_data[key][0]);

            let end = parseInt(season_data[key][1]);

            const Batsmans_data = {};

            for (
              let deliveries_index = 0;
              deliveries_index < Deliveries_data.length;
              deliveries_index++
            ) {
              let match_id = Deliveries_data[deliveries_index]["match_id"];

              let batsman = Deliveries_data[deliveries_index]["batsman"];

              let runs = parseInt(
                Deliveries_data[deliveries_index]["batsman_runs"]
              );

              if (match_id >= start && match_id <= end) {
                if (Batsmans_data[batsman]) {
                  let temp = Batsmans_data[batsman];

                  temp[0] += runs;

                  temp[1] += 1;

                  Batsmans_data[batsman] = temp;
                } else {
                  let arr = new Array(2);

                  arr[0] = runs;

                  arr[1] = 1;

                  Batsmans_data[batsman] = arr;
                }
              }
            }

            const final = {};
            for (let key2 in Batsmans_data) {
              if (final[key2] === undefined) {
                let strike =
                  (parseInt(Batsmans_data[key2][0]) /
                    parseInt(Batsmans_data[key2][1])) *
                  100;

                final[key2] = parseInt(strike.toFixed(2));
              }
            }

            Batsmans_strikerate_per_season[key] = final;
          }
        }

        fs.writeFileSync(
          "../public/output/7-strike-rate-of-batsman-per-season.json",
          JSON.stringify(Batsmans_strikerate_per_season, null, 4)
        );
      });
  });
