const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Deliveries_data = [];

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Deliveries_data.push(data))
  .on("end", () => {
    const bowlers_data = {};

    for (let index = 0; index < Deliveries_data.length; index++) {
      const runs = parseInt(Deliveries_data[index]["total_runs"]);

      if (Deliveries_data[index]["is_super_over"] != 0) {
        const Bowler = Deliveries_data[index]["bowler"];

        if (bowlers_data.hasOwnProperty(Bowler)) {
          let temp = bowlers_data[Bowler];

          temp[0] += runs;

          temp[1] += 1;

          bowlers_data[Bowler] = temp;
        } else {
          const data = new Array(2);

          data[0] = runs;

          data[1] = 1;

          bowlers_data[Bowler] = data;
        }
      }
    }
    const Economical_bowler = {};

    let min = 0;

    let topper = "";

    for (let key in bowlers_data) {
      const sb = {};

      let Economy = bowlers_data[key][0] / (bowlers_data[key][1] / 6);

      if (min < Economy) {
        min = Economy;

        topper = key;
      }
    }
    Economical_bowler[topper] = min;

    fs.writeFileSync(
      "../public/output/9-bowler-with-best-economy-in-superover.json",
      JSON.stringify(Economical_bowler, null, 4)
    );
  });
