const { match } = require("assert");
const csv = require("csv-parser");
const fs = require("fs");
const Matches_data = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv({ separator: "," }))
  .on("data", (data) => Matches_data.push(data))
  .on("end", () => {
    const Tossandmatchwinner = {};

    for (let index = 0; index < Matches_data.length; index++) {
      let toss_winner = Matches_data[index]["toss_winner"];
      let match_winner = Matches_data[index]["winner"];

      if (toss_winner === match_winner) {
        if (Tossandmatchwinner[toss_winner]) {
          Tossandmatchwinner[toss_winner]++;
        } else {
          Tossandmatchwinner[toss_winner] = 1;
        }
      }
    }

    fs.writeFileSync(
      "../public/output/5-Total-times-match-and-toss-winner.json",
      JSON.stringify(Tossandmatchwinner, null, 4)
    );
  });
